class _animus_fld 
{
class   _base_fld // Inherited class at offset 0x0
{

          unsigned long	m_dwIndex;		 // this+0x0
          char[0x40]	m_strCode;		 // this+0x4

};

        int	m_nLevel;		 // this+0x44
        unsigned __int64	m_nForLvUpExp;		 // this+0x48
        int	m_nUseFP;		 // this+0x50
        float	m_fPenalty;		 // this+0x54
        float	m_fAttGap;		 // this+0x58
        int	m_nAttack_DP;		 // this+0x5C
        int	m_nAttFcStd;		 // this+0x60
        int	m_nMinAFSelProb;		 // this+0x64
        int	m_nMaxAFSelProb;		 // this+0x68
        int	m_nAttSklUnit;		 // this+0x6C
        int	m_nDefSklUnit;		 // this+0x70
        float	m_fWeakPart;		 // this+0x74
        int	m_nStdDefFc;		 // this+0x78
        float	m_fDefGap;		 // this+0x7C
        float	m_fDefFacing;		 // this+0x80
        int	m_nFireTol;		 // this+0x84
        int	m_nWaterTol;		 // this+0x88
        int	m_nSoilTol;		 // this+0x8C
        int	m_nWindTol;		 // this+0x90
        int	m_nForceLevel;		 // this+0x94
        int	m_nForceMastery;		 // this+0x98
        int	m_nForceAttStd;		 // this+0x9C
        char[0x40]	m_strAttTechID1;		 // this+0xA0
        int	m_nAttTech1UseProb;		 // this+0xE0
        int	m_nAttTechID1MotionTime;		 // this+0xE4
        char[0x40]	m_strPSecTechID;		 // this+0xE8
        int	m_nPSecTechIDMotionTime;		 // this+0x128
        char[0x40]	m_strMSecTechID;		 // this+0x12C
        int	m_nMSecTechIDMotionTime;		 // this+0x16C
        int	m_nMaxHP;		 // this+0x170
        int	m_nHPRecDelay;		 // this+0x174
        int	m_nHPRecUnit;		 // this+0x178
        int	m_nMaxFP;		 // this+0x17C
        int	m_nFPRecDelay;		 // this+0x180
        int	m_nFPRecUnit;		 // this+0x184
        int	m_nAttSpd;		 // this+0x188
        int	m_nAttMoTime1;		 // this+0x18C
        int	m_nAttMoTime2;		 // this+0x190
        int	m_nCrtMoTime;		 // this+0x194
        int	m_nViewExt;		 // this+0x198
        int	m_nRefExt;		 // this+0x19C
        int	m_nAttExt;		 // this+0x1A0
        int	m_nMovSpd;		 // this+0x1A4
        int	m_nScaleRate;		 // this+0x1A8
        int	m_nWidth;		 // this+0x1AC
        int	m_nAttEffType;		 // this+0x1B0
        int	m_nDefEffType;		 // this+0x1B4
        
};
