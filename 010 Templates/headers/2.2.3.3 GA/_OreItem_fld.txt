class _OreItem_fld 
{
class   _base_fld // Inherited class at offset 0x0
{

          unsigned long	m_dwIndex;		 // this+0x0
          char[0x40]	m_strCode;		 // this+0x4

};

        int	m_bExist;		 // this+0x44
        char[0x40]	m_strModel;		 // this+0x48
        int	m_nIconIDX;		 // this+0x88
        char[0x40]	m_strName;		 // this+0x8C
        int	m_nOre_List;		 // this+0xCC
        int	m_nOre_Level;		 // this+0xD0
        unsigned long	m_dwOreProbability;		 // this+0xD4
        int	m_nKindClt;		 // this+0xD8
        int	m_nMoney;		 // this+0xDC
        int	m_nStdPrice;		 // this+0xE0
        int	m_nStdPoint;		 // this+0xE4
        int	m_nGoldPoint;		 // this+0xE8
        int	m_nKillPoint;		 // this+0xEC
        int	m_nProcPoint;		 // this+0xF0
        int	m_nProcessPoint;		 // this+0xF4
        int	m_nStoragePrice;		 // this+0xF8
        int	m_bSell;		 // this+0xFC
        int	m_bExchange;		 // this+0x100
        int	m_bGround;		 // this+0x104
        int	m_bStoragePossible;		 // this+0x108
        int	m_bUseableNormalAcc;		 // this+0x10C
        int	m_nmin_C_random;		 // this+0x110
        int	m_nmax_C_random;		 // this+0x114
        int	m_nProcessPrice;		 // this+0x118
        char[0x40]	m_strTooltipIndex;		 // this+0x11C
        int	m_bIsTime;		 // this+0x15C
        
};
