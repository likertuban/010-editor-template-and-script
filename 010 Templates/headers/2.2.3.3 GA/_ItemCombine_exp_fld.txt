class _ItemCombine_exp_fld 
{
class   _base_fld // Inherited class at offset 0x0
{

          unsigned long	m_dwIndex;		 // this+0x0
          char[0x40]	m_strCode;		 // this+0x4

};


enum   <unnamed-tag> int
{
          int	make_item_std_prob;		 // constant 0x2710

};
        unsigned long	m_dwCommit;		 // this+0x44
        char[0x40]	m_strCivil;		 // this+0x48
        int	m_bCombineExist;		 // this+0x88
        char[0x40]	m_strCombineType;		 // this+0x8C
        int	m_nFailOutItem;		 // this+0xCC

enum   <unnamed-tag> int
{
          int	max_material_num;		 // constant 0x5

};
class   _material 
{
          char[0x8]	m_itmPdMat;		 // this+0x0
          unsigned long	m_dwUpt;		 // this+0x8
          int	m_nDur;		 // this+0xC

};
        struct _ItemCombine_exp_fld::_material[0x5]	m_Material;		 // this+0xD0
        int	m_bSelectItem;		 // this+0x120
        int	m_nOperationCount;		 // this+0x124

enum   <unnamed-tag> int
{
          int	max_output_num;		 // constant 0x18

};
class   _output_list 
{
          char[0x8]	m_itmPdOutput;		 // this+0x0
          unsigned long	m_dwUpt;		 // this+0x8
          int	m_dwEffectType;		 // this+0xC
          unsigned long	m_dwResultEffectMsgCode;		 // this+0x10
          unsigned long	m_dwPdProp;		 // this+0x14
          int	m_nOutNum;		 // this+0x18

};
        struct _ItemCombine_exp_fld::_output_list[0x18]	m_listOutput;		 // this+0x128
        
};
