class _TOWNItem_fld 
{
class   _base_fld // Inherited class at offset 0x0
{

          unsigned long	m_dwIndex;		 // this+0x0
          char[0x40]	m_strCode;		 // this+0x4

};

        int	m_bExist;		 // this+0x44
        char[0x40]	m_strModel;		 // this+0x48
        int	m_nIconIDX;		 // this+0x88
        char[0x40]	m_strName;		 // this+0x8C
        int	m_nKindClt;		 // this+0xCC
        int	m_nFixPart;		 // this+0xD0
        char[0x40]	m_strCivil;		 // this+0xD4
        char[0x40]	m_strMapCode;		 // this+0x114
        char[0x40]	m_strDummyName;		 // this+0x154
        int	m_nMoney;		 // this+0x194
        int	m_nStdPrice;		 // this+0x198
        int	m_nStdPoint;		 // this+0x19C
        int	m_nStoragePrice;		 // this+0x1A0
        int	m_nLV;		 // this+0x1A4
        int	m_nUpLevelLim;		 // this+0x1A8
        int	m_bSell;		 // this+0x1AC
        int	m_bExchange;		 // this+0x1B0
        int	m_bGround;		 // this+0x1B4
        int	m_bStoragePossible;		 // this+0x1B8
        int	m_bUseableNormalAcc;		 // this+0x1BC
        char[0x40]	m_strTooltipIndex;		 // this+0x1C0
        int	m_bIsCash;		 // this+0x200
        int	m_nUsePCCash;		 // this+0x204
        int	m_bIsTime;		 // this+0x208
        
};
